import logo from "./logo.svg";
import "./App.css";
import { Component } from "react";
import { NumberList } from "./components/numberlist";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numberList: [],
      number: 10,
    };
  }
  addNumber = () => {
    this.setState({
      numberList: [...this.state.numberList, this.state.number],
    });
    this.setState({ number: this.state.number + 10 });
  };

  render() {
    return (
      <>
        <div className="App">
          <button onClick={this.addNumber}>Adicionar numero</button>
        </div>
        <header className="App">
          <NumberList list={this.state.numberList}></NumberList>
        </header>
      </>
    );
  }
}
export default App;
