import { Component } from "react";
import { Number } from "../number";

export class NumberList extends Component {
  render() {
    const { list } = this.props;
    return (
      <div>
        {list.map((item, index) => (
          <Number key={index} number={item} />
        ))}
      </div>
    );
  }
}
