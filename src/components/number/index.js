import { Component } from "react";

export class Number extends Component {
  render() {
    return (
      <div
        style={{
          borderWidth: "2PX",
          borderStyle: "solid",
          borderColor: "green",
        }}
      >
        {this.props.number}
      </div>
    );
  }
}
